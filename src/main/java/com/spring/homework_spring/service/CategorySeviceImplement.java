package com.spring.homework_spring.service;
import com.spring.homework_spring.repository.CatRepository;
import com.spring.homework_spring.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategorySeviceImplement implements CategoryService {
    private CatRepository catRepository;
    @Autowired
    public void setCatRepository(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @Override
    public boolean insert(Category cat) {
        boolean isInserted = catRepository.insert(cat);
        if (isInserted)
            return true;
        else
            return false;
    }

    @Override
    public List<Category> select() {
        return catRepository.select();
    }

    @Override
    public boolean update(Category cat) {
        return catRepository.update(cat);
    }

    @Override
    public boolean deleteById(int id) {
        return catRepository.deleteById(id);
    }
}


