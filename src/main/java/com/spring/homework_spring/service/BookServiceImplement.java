package com.spring.homework_spring.service;

import com.spring.homework_spring.model.Book;
import com.spring.homework_spring.model.BookFilter;
import com.spring.homework_spring.model.Category;
import com.spring.homework_spring.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookServiceImplement implements BookService {
    private BookRepository bookRepository;
    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public boolean insert(Book book) {
        boolean isInserted = bookRepository.insert(book);
        if (isInserted)
            return true;
        else
            return false;
    }

    @Override
    public List<Book> select() {
        return bookRepository.select();
    }

//    @Override
//    public List<Book> select(BookFilter bookFilter) {
//        return bookRepository.select(bookFilter);
//    }

    @Override
    public boolean update(Book book) {
        return bookRepository.update(book);
    }

    @Override
    public boolean deleteById(int id) {
        return bookRepository.deleteById(id);
    }

    @Override
    public Book findById(int id) {
        return bookRepository.findById(id);
    }
}
