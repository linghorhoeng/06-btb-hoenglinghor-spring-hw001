package com.spring.homework_spring.service;
import com.spring.homework_spring.model.Book;
import com.spring.homework_spring.model.Category;
import java.util.List;

public interface CategoryService {
    boolean insert(Category cat);
    List<Category> select();
    boolean update(Category cat);
    boolean deleteById(int id);

}

