package com.spring.homework_spring.service;

import com.spring.homework_spring.model.Book;
import com.spring.homework_spring.model.BookFilter;
import com.spring.homework_spring.model.Category;

import java.util.List;

public interface BookService {
    boolean insert(Book book);
    List<Book> select();
//    List<Book> select(BookFilter bookFilter);
    boolean update(Book book);
    boolean deleteById(int id);
    Book findById(int id);
}
