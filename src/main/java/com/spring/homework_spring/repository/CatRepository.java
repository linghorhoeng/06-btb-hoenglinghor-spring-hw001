package com.spring.homework_spring.repository;

import com.spring.homework_spring.model.Book;
import com.spring.homework_spring.model.Category;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CatRepository {
    @Insert("INSERT INTO tb_categories (title) " +
            "VALUES (#{title})")
    boolean insert(Category cat);

    @SelectProvider(value = CatProvider.class, method = "select")
    List<Category> select();
    @Update("update tb_categories set title = #{title}" +
            "where id = #{id}")
    boolean update(Category cat);
    @Delete("delete from tb_categories where id = #{id}")
    boolean deleteById(int id);
}
