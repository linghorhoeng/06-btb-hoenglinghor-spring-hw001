package com.spring.homework_spring.repository;

import com.spring.homework_spring.model.BookFilter;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {
    public String select(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_book");
        }}.toString();
    }

//    public String selectByFilter(BookFilter bookFilter){
//        return new SQL(){{
//            SELECT("*");
//            FROM("tb_book");
//            if(bookFilter.getBookTitle()!=null)
//                WHERE("title ILIKE '%' || #{title} || '%'");
//            if(bookFilter.getCat_id()!=0)
//                WHERE("cat_id = #{cat_id}");
//        }
//        }.toString();
//    }
}
