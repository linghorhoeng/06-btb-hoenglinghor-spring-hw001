package com.spring.homework_spring.repository;

import org.apache.ibatis.jdbc.SQL;

public class CatProvider {
    public String select(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_categories");
        }}.toString();
    }

}
