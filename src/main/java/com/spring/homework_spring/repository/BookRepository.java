package com.spring.homework_spring.repository;

import com.spring.homework_spring.model.Book;
import com.spring.homework_spring.model.BookFilter;
import com.spring.homework_spring.model.Category;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.security.Provider;
import java.util.List;
import java.util.logging.Filter;

@Repository
public interface BookRepository {
    @Insert("INSERT INTO tb_book (title,author,description,thumbnail,cat_id) " +
            "VALUES (#{title},#{author},#{description},#{thumbnail},#{cat_id})")
    boolean insert(Book book);
    @SelectProvider(value = BookProvider.class, method = "select")
    List<Book> select();
//    @SelectProvider(value = BookProvider.class, method = "selectByFilter")
//    List<Book> select(BookFilter bookFilter);
    @Update("update tb_book set title = #{title}, description = #{description} ,thumbnail =#{thumbnail},cat_id =#{cat_id}" +
         "where id = #{id}")
    boolean update(Book book);
    @Delete("delete from tb_book where id = #{id}")
    boolean deleteById(int id);
    @Select("select * from tb_book where id = #{id}")
    Book findById(int id);
}




