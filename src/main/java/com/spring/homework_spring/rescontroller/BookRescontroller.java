package com.spring.homework_spring.rescontroller;

import com.spring.homework_spring.model.Book;
import com.spring.homework_spring.model.BookFilter;
import com.spring.homework_spring.model.Category;
import com.spring.homework_spring.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
public class BookRescontroller {


@Autowired
BookService bookServiceImp;
//    public void setBookServiceImp(BookService bookServiceImp) {
//        this.bookServiceImp = bookServiceImp;
//    }

//    @GetMapping("/books")
//    public List<Book> findAll(BookFilter bookFilter){
//        return bookServiceImp.select(bookFilter);
//    }
    @GetMapping("/books")
    public List<Book> findAll(){
        return bookServiceImp.select();
    }
    @PostMapping("/books")
    public boolean insert(@RequestBody Book book){
        return bookServiceImp.insert(book);
    }
    @PutMapping("/books")
    public boolean update(@RequestBody Book book ){return bookServiceImp.update(book);}
    @DeleteMapping("/books/{id}")
    public boolean deleteById(@PathVariable int id){ return  bookServiceImp.deleteById(id);}
    @GetMapping("/books/{id}")
    public Book findById(@PathVariable int id){return bookServiceImp.findById(id);}
}
