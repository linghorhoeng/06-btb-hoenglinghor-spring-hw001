package com.spring.homework_spring.rescontroller;

import com.spring.homework_spring.model.Book;
import com.spring.homework_spring.service.CategorySeviceImplement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.spring.homework_spring.model.Category;

import java.util.List;

@RestController
@RequestMapping
public class CatRescontroller {
    @Autowired
    CategorySeviceImplement catServiceImp;

    @GetMapping("/categories")
    public List<Category> findAll(){
        return catServiceImp.select();
    }
    @PostMapping("/categories")
    public boolean insert(@RequestBody Category cat){
        return catServiceImp.insert(cat);
    }
    @PutMapping("/categories")
    public boolean update(@RequestBody Category cat ){return catServiceImp.update(cat);}
    @DeleteMapping("/categories/{id}")
    public boolean deleteById(@PathVariable int id){ return  catServiceImp.deleteById(id);}
}