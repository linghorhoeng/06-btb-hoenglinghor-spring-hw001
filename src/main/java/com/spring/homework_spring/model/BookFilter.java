package com.spring.homework_spring.model;

public class BookFilter {
    private String bookTitle;
    private int cat_id;

    public BookFilter(String bookTitle, int cat_id) {
        this.bookTitle = bookTitle;
        this.cat_id = cat_id;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "title='" + bookTitle + '\'' +
                ", categoryId=" + cat_id +
                '}';
    }
}
